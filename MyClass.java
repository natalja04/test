/**
 * Test class that implements calculator options.
 */
public class MyClass {
	/**
	 * Method that returns the sum of arguments
	 * @param a the first argument
	 * @param b the second argument
	 * @return int The result
	 */
	public int add(int a, int b){
		return a + b;
	}
	
	/**
	 * Method that returns sum of arguments expressed as String
	 * @param a the first argument
	 * @param b the second argument
	 * @return String The sum of strings
	 */
	public String add(String a, String b){
		return a + b;
	}
	
	/**
	 * Method that returns difference between two arguments
	 * @param a the first argument
	 * @param b the second argument
	 * @return int The result
	 */
	public int sub(int a, int b){
		return a - b;
	}
	
	/**
	 * Method that returns the multiplication of two arguments
	 * @param a the first argument
	 * @param b the second argument
	 * @return int The result
	 */
	public int mult(int a, int b){
		return a * b;
	}
	
	/**
	 * Method that returns the division of two arguments
	 * @param a the first argument
	 * @param b the second argument
	 * @return int The result
	 */
	public int div(int a, int b){
		return a / b;
	}
	
	/**
	 * Method that splits the left operand to the right operand and returns the remainder
	 * @param a the first argument
	 * @param b the second argument
	 * @return int The result
	 */
	public int mod(int a, int b){
		return a%b;
	}
	/**
	 * Method that returns greeting message.
	 * @param name variable, whose name should be welcomed
	 * @return welcome string like "Hello user".
	 */
	public String sayHello(String name){
		return "Hello" + name;
	}
}
